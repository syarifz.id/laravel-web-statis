<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home() {
        return view('hal.index');
    }

    public function datatable() {
        return view('hal.datatable');
    }
}
