@extends('layout.master')
@section('title')
Buat Account Baru
@endsection
@section('title2')
Sign Up Form
@endsection
@section('content')
        <form action="/welcome" method="POST">
            @csrf
            <label>First Name</label><br>
            <input type="text" name="firstname"><br><br>
            <label>Last Name</label><br>
            <input type="text" name="lastname"><br><br>
            <label>Gender</label><br>
            <input type="radio" name="gender" id="gender" value="male">Male<br>
            <input type="radio" name="gender" id="gender" value="female">Female<br><br>
            <label>Nationalisty</label><br>
            <select name="nationality">
                <option value="indonesia">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="arabian">Arabian</option>
            </select><br><br>
            <label>Language Spoken</label><br>
            <input type="checkbox" name="language" value="indonesia">Indonesia<br>
            <input type="checkbox" name="language" value="melayu">Melayu<br>
            <input type="checkbox" name="language" value="english">English<br><br>
            <label>Bio</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>
            <input type="submit" value="Sign Up">
        </form>
@endsection
