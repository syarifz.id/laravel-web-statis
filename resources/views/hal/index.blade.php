@extends('layout.master')
@section('title')
Media Online
@endsection
@section('title2')
Sosial Media Developer
@endsection
@section('content')
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h2>Benefit Join di Media Online</h2>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h2>Cara Bergabung ke Media Online</h2>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection

